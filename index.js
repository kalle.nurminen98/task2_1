const express = require("express")
const { dirname } = require("path")
const app = express()
// Destructuring from an object
const {PORT = 8080} = process.env
const path = require("path")

app.use(express.static(path.join(__dirname, "public")))

app.get("/", function(req, res){

    return res.sendFile(path.join(__dirname, "public", "index.html"))
})

app.listen(PORT, function(){
    console.log("Listening port " + PORT)
})