## Table of Contents

- [Sections](#sections)
  - [Title](#title)
  - [Short Description](#short-description)
  - [Install](#install)
  - [Usage](#usage)
  - [Maintainers](#maintainers)
  - [Contributing](#contributing)
  - [License](#license)

## Sections

### Title

Website

### Short Description

Website that has picture of train and list of different dog and cat animals.

### Install

npm install

### Usage

npm run dev

### Maintainer(s)

Kalle Nurminen @kalle.nurminen98

### Contributing

Kalle Nurminen @kalle.nurminen98

### License

Free to use. No license
